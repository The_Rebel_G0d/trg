const _ = require("lodash"),
    util = require("util"),
    cli = require("cli"),
    loggy = require("loggy"),
    initSkeleton = require("init-skeleton").init,
    strings = require("./lib/strings.json");
require("promise.prototype.finally").shim();

cli.parse(null, ["new", "node", "electron", "list"]);
const skeletons = {
    electron: "https://bitbucket.org/The_Rebel_G0d/electron_skeleton",
    node: "https://bitbucket.org/The_Rebel_G0d/node_skeleton"
};

switch (cli.command) {
    case "new":
        if (cli.args.length < 2) {
            loggy.error("Missing type. Usage: trg new [electron|node] folder_path");
        } else {
            switch (cli.args[0].toLowerCase().trim()) {
                case "electron":
                    newElectron(cli.args[1]);
                    break;
                case "node":
                    newNode(cli.args[1]);
                    break;
                default:
                    loggy.error(`Invalid project type: "${cli.args[0]}". Expecting: [electron|node]. Usage: trg new [electron|node] folder_path`);
                    break;
            }
        }
        break;
    case "node":
        if (cli.args.length > 0) newNode(cli.args[0]); else loggy.error(`Missing new project path. Usage: trg node folder_path`);
        break;
    case "electron":
        if (cli.args.length > 0) newElectron(cli.args[0]); else loggy.error(`Missing new project path. Usage: trg electron folder_path`);
        break;
    case "list":
        loggy.info(JSON.stringify(skeletons, null, 4));
        break;
    default:
        loggy.error(`Invalid command: "${cli.command}". Usage: trg new [electron|node] folder_path`);
        break;
}

function prereqs(path) {
    return new Promise((res) => {
        loggy.info("Prereqs running on path:", path);

        require("mkdirp").sync(path);
        require("mkdirp").sync(require("path").join(path, ".idea"));

        res();
    });
}

function newElectron(path) {
    prereqs(path).then(() => {
        initSkeleton(skeletons.electron, {logger: loggy, rootPath: path}, err => {
            if (err) {
                loggy.error(err);
            } else {
                loggy.info("Electron project exported, modifying jetbrains project...");

                _.each(Object.keys(strings.electron.files), key => {
                    require("fs").writeFileSync(require("path").join(path, ".idea/", key), atob(strings["electron"]["files"][key]));
                    loggy.info(`Wrote ${key}`);
                });

                loggy.info("All done! Happy coding.");
            }
        });
    });
}

function newNode(path) {
    prereqs(path).then(() => {
        initSkeleton(skeletons.node, {logger: loggy, rootPath: path}, err => {
            if (err) {
                loggy.error(err);
            } else {
                loggy.info("Node project exported, modifying jetbrains files...");

                _.each(Object.keys(strings.node.files), key => {
                    require("fs").writeFileSync(require("path").join(path, ".idea/", key), atob(strings["node"]["files"][key]));
                    loggy.info(`Wrote .idea/${key}`);
                });

                loggy.info("All done! Happy coding.");
            }
        });
    });
}

function atob(str) {
    return new Buffer(str, "Base64").toString();
}